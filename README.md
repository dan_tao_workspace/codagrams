# codagrams

Vision:

```
# Generate diagrams from code in a project
codagram

# Used by CI to ensure the generated diagrams in a project match the code
codagram --validate
```

Workflow:

1. Developer submits a pull request containing a change to some diagram code
   as well as updated images
2. A passing build (using `codagram --validate`) informs reviewers that the
   images match the code
3. This way a team can maintain an authoritative set of architectural diagrams
   in version control

To do:

- [x] Seed this project with an example diagram source file
- [x] Generate a matching image file using a CLI tool
- [x] Implement a test to validate the generated image matches the source
- [x] Configure CI for this project
